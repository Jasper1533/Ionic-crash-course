import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Observable } from '../../../node_modules/rxjs/Observable';

/**
 * Generated class for the PeopleDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-people-details',
  templateUrl: 'people-details.html',
})
export class PeopleDetailsPage {
  person: any;
  films: Array<any> = [];
  film: Observable<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiProvider: ApiProvider) {
    this.person = navParams.get('person');
    this.getFilms(this.person.films);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PeopleDetailsPage');
  }

  getFilms(films, index:number = 0){
    if(films[index] !== undefined){
      this.film = this.apiProvider.getSpecific(films[index]); 
      this.film.subscribe(data => this.films.push(data));
      this.getFilms(films, index+1);
    }
  }

  openDetails(film){
    this.navCtrl.push('FilmDetailsPage', {film: film});
  }

}
